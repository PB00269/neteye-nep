%include continuous-integration/build/spec_common_variables.inc

%define nep_project_dir         nep
%define nep_project_doc_dir     %{nep_project_dir}/doc/

%define neteye_nep_dir          %{ne_dir}/nep/

Name:    neteye-nep
Version: %{automatic_rpm_version}
Release: %{automatic_rpm_release}
Summary: neteye-nep Package

Group:	 Applications/System
License: GPL v3
Source0: %{name}.tar.gz
BuildArch: noarch


%description
%{summary}

%prep
%setup -c

%build

%install
mkdir -p %{buildroot}/%{neteye_nep_dir}

# Do not ship the directory /doc/ of the nep project. It is only used for the online userguide
rm -rf %{nep_project_doc_dir}
mv %{nep_project_dir}/* %{buildroot}/%{neteye_nep_dir}

%files
%{neteye_nep_dir}

%changelog
%{automatic_rpm_changelog}
- Latest build of %{name}